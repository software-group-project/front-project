"use strict";

import Vue from "vue";
import axios from "axios";
import router from "./router";
import store from "../store";

let config = {
  baseURL: process.env.VUE_APP_BACKEND,
};
const _axios = axios.create(config);
_axios.urls = {
  test: "12",
};
_axios.interceptors.request.use(
  function (config) {
    let token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = `Token ${token}`;
      store.commit("setUser");
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    return response;
  },
  function (error) {
    // Do something with response error
    if (error.response.status == 401) {
      localStorage.removeItem("token");
      router.push("/login");
      store.commit("logout");
    }
    return Promise.reject(error);
  }
);
export default _axios;
