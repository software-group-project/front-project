import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

//polymorohism
//for duplicated router
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    redirect: "/profile",
  },

  {
    path: "/login-prime",
    name: "loginPrime",
    component: () => import("@/views/LoginPrime.vue"),
    meta: {
      dontNeedAuth: true,
    },
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/Login.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/forget-password",
    name: "ForgetPassword",
    component: () => import("@/views/ForgetPassword.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/change-password",
    name: "ChangePassword",
    component: () => import("@/views/ChangePassword.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("@/views/Signup.vue"),
    meta: {
      guest: true,
    },
  }, {
    path: "/admin-login",
    name: "adminPanel",
    component: () => import("@/views/AdminLogin.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/profile",
    name: "profile",
    component: () => import("@/views/Profile.vue"),
  },
  // {
  //   path: "/accesses",
  //   name: "accesses",
  //   component: () => import("@/views/Accesses.vue"),
  // },
  {
    path: "/product",
    name: "product",
    component: {
      render(c) {
        return c("router-view");
      },
    },
    children: [
      {
        path: "list",
        name: "listProduct",
        component: () => import("@/views/product/ListProduct.vue"),
        meta: {
          dontNeedAuth: true,
        },
      },
      {
        path: "detail/:productId",
        name: "detailProduct",
        component: () => import("@/views/product/DetailProduct.vue"),
      },
      {
        path: "owner-list",
        name: "ownerProducts",
        component: () => import("@/views/product/OwnerProducts.vue"),
      },
    ],
  },
  {
    path: "/admin",
    name: "Admin",
    component: {
      render(c) {
        return c("router-view");
      },
    },
    children: [
      {
        path: "users",
        name: "userList",
        component: () => import("@/views/admin/UserList.vue"),
        meta: {
          dontNeedAuth: true,
        },
      },
    ],
  },
  {
    path: "/cart",
    name: "cart",
    component: () => import("@/views/cart/CartList.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.hasOwnProperty("guest")) {
    if (localStorage.getItem("token")) {
      next(from.path);
    } else {
      next();
    }
  } else if (to.meta.hasOwnProperty("dontNeedAuth")) {
    next();
  } else {
    if (localStorage.getItem("token")) {
      next();
    } else {
      next(from.path);
    }
  }
});

export default router;
