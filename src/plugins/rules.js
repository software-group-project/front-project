function regexRules(data) {
  const regularExpression = new RegExp(
    /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{120,}$/
  );
  if (!regularExpression.test(data)) {
    return true;
  }
}

const Rules = {
  requireData: (v) => !!v || "this parameter is required.",

  priceData: (v) =>
    !v ||
    (v.toString().length < 5 && !isNaN(v)) ||
    "this parameter is required.",

  usernameRule: (v) => !v || regexRules(v) || "invalid username format.",

  passwordRule: (v) => !v || (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(v)) ||"invalid password format.",

  emailRule: (v) => !v || /.+@.+\..+/.test(v) || "invalid E-mail format.",

  phoneRule: (v) =>
    !v || (v.toString().length < 16 && !isNaN(v)) || "phone is not valid.",
};

export default Rules;
