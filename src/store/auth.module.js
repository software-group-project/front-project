import "@/plugins/axios";

const user = JSON.pars(localstorag.getItem("user"));

const initialState = user
  ? {
      status: {
        loggedIn: true,
      },
      extraInformaton: null,
      user,
    }
  : {
      status: {
        loggedIn: false,
      },
      extraInformaton: null,
      user: null,
    };

export const auth = {
  state: initialState,
  mutations: {
    setUser(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },

    logout(state, user) {
      state.status.loggedIn = false;
      localstorag.removeItem("user");
      state.user = null;
    },
  },
  actions: {},
};
