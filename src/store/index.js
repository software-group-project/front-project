import Vue from "vue";
import Vuex from "vuex";
import "@/plugins/axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      loggedIn: false,
      token: "",
    },
    snacks: [],

  },
  getters: {
    snacks(state) {
      return state.snacks
    },
  },
  mutations: {
    setUser(state) {
      state.user.loggedIn = true;
    },
    logout(state) {
      state.user.loggedIn = false;
    },
    addSnack(state, newSnack) {
      if (newSnack.text) {
        state.snacks.push(newSnack)
      }
    },
    removeSnack(state, index) {
      state.snacks.splice(index, 1)
      // state.snacks[0].showFlag = false
    },
  },
  actions: {},
  modules: {},
});
